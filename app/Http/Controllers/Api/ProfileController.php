<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    public static function rules(Request $request = null)
    {
        return [
            'store' => [
                'wantedJobTitle' => 'string|max:255|nullable',
                'firstName' => 'string|max:255|nullable',
                'lastName' => 'string|max:255|nullable',
                'email' => 'required|email|unique:users',
                'phone' => 'string|max:255|nullable',
                'country' => 'string|max:255|nullable',
                'city' => 'string|max:255|nullable',
                'address' => 'string|max:255|nullable',
                'postalCode' => 'numeric|nullable',
                'drivingLicence' => 'string|max:255|nullable',
                'nationality' => 'string|max:255|nullable',
                'placeOfBirth' => 'string|max:255|nullable',
                'dateOfBirth' => 'date_format:d-m-Y|nullable',
            ],
            'update' => [
                'wantedJobTitle' => 'string|max:255|nullable',
                'firstName' => 'string|max:255|nullable',
                'lastName' => 'string|max:255|nullable',
                'phone' => 'string|max:255|nullable',
                'country' => 'string|max:255|nullable',
                'city' => 'string|max:255|nullable',
                'address' => 'string|max:255|nullable',
                'postalCode' => 'numeric|nullable',
                'drivingLicence' => 'string|max:255|nullable',
                'nationality' => 'string|max:255|nullable',
                'placeOfBirth' => 'string|max:255|nullable',
                'dateOfBirth' => 'date_format:d-m-Y|nullable',
            ]
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate(self::rules($request)['store']);

        $profile = new User;
        $profile->profileCode = mt_rand(10000000,99999999);
        foreach (self::rules($request)['store'] as $key => $value) {
            if (Str::contains($value, [ 'file', 'image', 'mimetypes', 'mimes' ])) {
                if ($request->hasFile($key)) {
                    $profile->{$key} = $request->file($key)->store('profile', 'public');
                } elseif ($request->exists($key)) {
                    $profile->{$key} = $request->{$key};
                }
            } elseif ($request->exists($key)) {
                $profile->{$key} = $request->{$key};
            }
        }
        try {
            DB::beginTransaction();
            $profile->save();
            $photo = new File;
            $photo->profileCode = $profile["profileCode"];
            $photo->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        return response()->json(["profileCode" => $profile["profileCode"]], 200);
    }

    public function show($id)
    {
        $profile = User::query()->where('profileCode', $id)->first();
        if (!$profile) abort(404);
        return response()->json($profile, 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate(self::rules($request)['update']);
        $profile = User::query()->where('profileCode', $id)->first();
        if (!$profile) abort(404);
        foreach (self::rules($request)['update'] as $key => $value) {
            if (Str::contains($value, [ 'file', 'image', 'mimetypes', 'mimes' ])) {
                if ($request->hasFile($key)) {
                    $profile->{$key} = $request->file($key)->store('profile', 'public');
                } elseif ($request->exists($key)) {
                    $profile->{$key} = $request->{$key};
                }
            } elseif ($request->exists($key)) {
                $profile->{$key} = $request->{$key};
            }
        }
        $profile->save();
        return response()->json(["profileCode" => $profile["profileCode"]], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
