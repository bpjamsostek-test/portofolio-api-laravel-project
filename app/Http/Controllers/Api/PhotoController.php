<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $photo = File::query()->where('profileCode', $id)->first();
        if (!$photo) abort(404);
        $filepath = $photo['path'];
        if ($filepath && file_exists(public_path($filepath))){
            $filetype = pathinfo($filepath, PATHINFO_EXTENSION);
            if ($filetype==='svg'){
                $filetype .= '+xml';
            }
            $get_img = file_get_contents(public_path($filepath));
            return 'data:image/' . $filetype . ';base64,' . base64_encode($get_img );
        }
        return null;
    }

    public function update(Request $request, $id)
    {
        $profile = User::query()->where('profileCode', $id)->first();
        if (!$profile) abort(404);
        $photo = File::query()->where('profileCode', $id)->first();
        if (!$photo) abort(404);
        $request->validate(['base64img' => 'required|string']);
        $base64 = $request->base64img;
        $extension = explode('/', mime_content_type($base64))[1];
        $name = $id.".".$extension;
        $path = "/upload/photo/";
        Image::make(file_get_contents($base64))->save(public_path($path).$name);

        $photo->name= $name;
        $photo->path= $path.$name;
        $photo->url= URL::to("/upload/photo/".$name);
        $photo->extension= $extension;
        try {
            DB::beginTransaction();
            $photo->save();

            $profile->photoUrl = $photo["url"];
            $profile->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        return response()->json([
            "profileCode" => $photo["profileCode"],
            "photoUrl" => $photo["url"]
        ], 200);
    }

    public function destroy($id)
    {
        $profile = User::query()->where('profileCode', $id)->first();
        if (!$profile) abort(404);
        $photo = File::query()->where('profileCode', $id)->first();
        if (!$photo) abort(404);
        $photo->name= null;
        $photo->path= null;
        $photo->url= null;
        $photo->extension= null;
        try {
            DB::beginTransaction();
            $photo->save();

            $profile->photoUrl = null;
            $profile->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        return response()->json([
            "profileCode" => $photo["profileCode"]
        ], 200);
    }
}
